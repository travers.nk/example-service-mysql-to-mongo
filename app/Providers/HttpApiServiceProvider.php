<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use App\Services\HttpApiService;

class HttpApiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \App::bind('HttpApiService', function() {
            return new HttpApiService;
        });
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}