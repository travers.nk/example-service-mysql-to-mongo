<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeJsonStrProductsNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products_new', function (Blueprint $table) {
            $table->dropColumn('ids_category_json', 'ids_images_json','ids_features_json');
            $table->json('ids_category_str');
            $table->json('ids_images_str');
            $table->json('ids_features_str');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_new', function (Blueprint $table) {
            //
        });
    }
}
