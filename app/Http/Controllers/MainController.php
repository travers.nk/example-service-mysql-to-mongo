<?php

namespace App\Http\Controllers;
use App\Category;
use App\Product;
use Illuminate\Routing\Controller as BaseController;

ini_set('max_execution_time', 900);


class MainController extends BaseController
{
    public function hello()
    {
        return view('main/hello');
    }
}
