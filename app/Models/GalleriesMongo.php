<?php

namespace App\Models;

//use Moloquent\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class GalleriesMongo extends Eloquent {

    protected $connection = 'mongodb';
    protected $collection = 'galleries_presta';

    static function makeGalleryMongo($alt, $name, $type, $products){
        $galleriesMongo = new GalleriesMongo();
        $galleriesMongo->alt = $alt;
        $galleriesMongo->name = $name;
        $galleriesMongo->type = $type;
        $galleriesMongo->products = $products;
        $galleriesMongo->save();
    }
}