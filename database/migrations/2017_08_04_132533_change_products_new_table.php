<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductsNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products_new', function (Blueprint $table) {
            $table->dropColumn('ids_category', 'ids_images','ids_features');
            $table->json('ids_category_json');
            $table->json('ids_images_json');
            $table->json('ids_features_json');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_new', function (Blueprint $table) {
            //
        });
    }
}
