<?php

Route::get('/', 'MainController@hello');
// ================== refactor routes ===================

Route::get('/get-products', 'HttpApiController@getProducts');
Route::get('/get-cats', 'HttpApiController@getCategories');
Route::get('/get-feats', 'HttpApiController@getProductFeatures');
Route::get('/get-feats-val', 'HttpApiController@getProductFeaturesValues');
Route::get('/get-tax', 'HttpApiController@getTax');
    //->middleware('auth.basic');
Route::get('/get-manuf', 'HttpApiController@getManufacturer');
Route::get('/get-stock', 'HttpApiController@getStock');
Route::get('/get-country', 'HttpApiController@getCountry');

//getStockQties
Route::get('/get-all', 'HttpApiController@getAll');

Route::get('/make-products', 'MongoController@makeProductMongo');
Route::get('/make-galls', 'MongoController@makeGalleryMongo');
Route::get('/make-cats', 'MongoController@makeCategoryMongo');
Route::get('/make-countries', 'MongoController@makeCountryMongo');

Route::get('/make-all', 'MongoController@makeAll');

Route::post('/update-product', 'MongoController@updateProduct')->middleware('auth-header');
Route::post('/delete-product', 'MongoController@deleteProduct')->middleware('auth-header');
Route::post('/add-product', 'MongoController@addProduct')->middleware('auth-header');


Route::get('/test', 'MongoController@test');

//email: mobile-app@app.com
//passwd: peyrouse-mobile-app