<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

        if ($e instanceof AuthenticationException) {
            return response('You do not have valid credentials', 401);
        }

        if ($e instanceof NotFoundHttpException){
            return response('Resource not found', 404);
        }

/*        if ($e instanceof Exception){
            return response($e->getMessage(), 500);
        }

        if ($e instanceof \PDOException) {
            if (strtolower(env('APP_ENV')) == 'local') {
                return response($e->getMessage(), 500);
            } else {
                return response('Internal Server Error', 500);
            }
        }*/

        if ($e instanceof ModelNotFoundException) {
            $modelPathAsArray = explode('\\', $e->getModel());
            $model = $modelPathAsArray[count($modelPathAsArray) - 1];
            return response($model . ' not found', 404);
        }

        return parent::render($request, $e);
    }
}
