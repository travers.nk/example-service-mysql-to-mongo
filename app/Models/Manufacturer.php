<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    protected $table = 'manufacturer';

    static function createManuf($pro_desc){
        $manuf = new Manufacturer();
        $manuf->ps_manuf_id = (int)$pro_desc->manufacturer->id;
        $manuf->name =  $pro_desc->manufacturer->name;
        $manuf->save();
    }
}