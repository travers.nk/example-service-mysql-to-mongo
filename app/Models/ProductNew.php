<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProductNew extends Model
{
    protected $table = 'products_new';
    protected $connection = 'mysql';

    private static function setData($response, $prod){
        $pro_desc = $response['pro_desc'];
        $cat_str = $response['cat_str'];
        $img_str = $response['img_str'];
        $feat_str = $response['feat_str'];

        $prod->ps_product_id =        (int)$pro_desc->product->id;
        $prod->id_manufacturer =      (int)$pro_desc->product->id_manufacturer;
        $prod->id_category_default =  (int)$pro_desc->product->id_category_default;
        $prod->id_default_image =     (int)$pro_desc->product->id_default_image;
        $prod->id_tax_rules_group =   (int)$pro_desc->product->id_tax_rules_group;
        $prod->position_in_category = (int)$pro_desc->product->position_in_category;
        $prod->manufacturer_name =    $pro_desc->product->manufacturer_name;
        $prod->quantity =             (float)$pro_desc->product->quantity;
        $prod->id_shop_default =      (int)$pro_desc->product->id_shop_default;
        $prod->reference =            $pro_desc->product->reference;
        $prod->ean13 =                $pro_desc->product->ean13;
        $prod->price =                (float)$pro_desc->product->price;
        $prod->date_add =             $pro_desc->product->date_add;
        $prod->date_upd =             $pro_desc->product->date_upd;
        $prod->meta_description =     $pro_desc->product->meta_description->language;
        $prod->meta_title =           $pro_desc->product->meta_title->language;
        $prod->name =                 $pro_desc->product->name->language;
        $prod->description =          $pro_desc->product->description->language;
        $prod->description_short =    $pro_desc->product->description_short->language;
        $prod->action =               $pro_desc->product->action->language;
        $prod->application =          $pro_desc->product->application->language;
        $prod->ean13_2 =              $pro_desc->product->ean13_2;
        $prod->h1_prod =              $pro_desc->product->h1_prod->language;
        $prod->prix_public =          (float) $pro_desc->product->prix_public->language;
        $prod->ids_category_char = ltrim($cat_str, ';');
        $prod->ids_images_char = ltrim($img_str, ';');
        $prod->ids_features_char = ltrim($feat_str, ';');
        $prod->active =               $pro_desc->product->active;
        //dd($pro_desc->product->associations->stock_availables->stock_available->id);
        $prod->id_stock =               $pro_desc->product->associations->stock_availables->stock_available->id;

        return $prod;

    }

    static function createProduct($response){
        $prod = self::setData($response, new ProductNew());
        $prod->save();
        return $prod;
    }

    static function updateProduct($response){
        $prod = self::setData(
            $response,
            ProductNew::where('ps_product_id', $response['pro_desc']->product->id)->firstOrFail()
        );
        $prod->save();
        return ($prod);
    }

    static function deleteProduct($ps_product_id){
        ProductNew::where('ps_product_id', $ps_product_id)->delete();
    }
}