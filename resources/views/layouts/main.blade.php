<html>
<head>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
@section('sidebar')
    <nav class="navbar navbar-default">
        <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li> <a href="#"> 1000 Geeks for mobile :)</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Get from Presta
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{URL::to('get-all')}}">Get all</a></li>
                    <li><a href="{{URL::to('get-cats')}}">Get categories</a></li>
                    <li><a href="{{URL::to('get-feats')}}">Get features</a></li>
                    <li><a href="{{URL::to('get-feats-val')}}">Get features values</a></li>
                    <li><a href="{{URL::to('get-tax')}}">Get tax</a></li>
                    <li><a href="{{URL::to('get-manuf')}}">Get manufacturers</a></li>
                    <li><a href="{{URL::to('get-products')}}">Get products</a></li>
                    <li><a href="{{URL::to('get-stock')}}">Get stock</a></li>
                    <li><a href="{{URL::to('get-country')}}">Get country</a></li>

                </ul>
            </li>

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Make in Mongo
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{URL::to('make-all')}}">Make all</a></li>
                    <li><a href="{{URL::to('make-products')}}">Make products</a></li>
                    <li><a href="{{URL::to('make-cats')}}">Make categories</a></li>
                    <li><a href="{{URL::to('make-galls')}}">Make galleries</a></li>
                    <li><a href="{{URL::to('make-countries')}}">Make countries</a></li>
                </ul>
            </li>
        </ul>
        </div>
    </nav>
@show

<div class="container">
    @yield('content')
</div>
</body>
</html>