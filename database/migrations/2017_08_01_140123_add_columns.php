<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->float('price_public');
            $table->char('ean13');
            $table->char('reference');
            $table->float('tax_rate');
            $table->char('imgUrlPresta');
            $table->char('brands');
            $table->char('format');
            $table->char('product_type');
            $table->char('hair');
            $table->char('gender');
            $table->char('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
