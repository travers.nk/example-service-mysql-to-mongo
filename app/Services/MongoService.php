<?php

namespace App\Services;

use App\Models\CartMongo;
use App\Models\FavoritesMongo;
use App\Models\CategoryMongo;
use App\Models\CountryMongo;
use App\Models\ProductMongo;
use App\Models\GalleriesMongo;
use App\Models\ProductNew;
use App\Models\ProductFeatures;
use App\Models\ProductFeaturesValues;
use App\Models\CategoryNew;
use App\Models\Tax;
use App\Models\Stock;
use App\Models\Country;

class MongoService
{
    // products id in ps_cat_id = 2 - topWeek
    //  products id in ps_cat_id = 393 - topSale
    // products id in ps_cat_id = 394 - New
    private  $galleries = [
        [2,   "top-week", "Coup de cœur", "main"],
        [393, "top-sale", "Meilleures ventes", "main"],
        [394, "new",  "Nouveautés", "main"],
    ];

    private function getCategories($product){
        $categories = explode(';', $product->ids_category_char);
        $categoryMongo = [];
        $categoryMongoObj = [];
        $categoryDraft = [];
        foreach ($categories as $cat){
            $curr_cat = CategoryNew::where('ps_cat_id', (int)$cat)->first();
            $curr_cat_parent = $curr_cat;
            //==============================
            if (is_object($curr_cat_parent)) {
                while ($curr_cat_parent->parent_id >= 2) {
                    if ($curr_cat_parent->type) {
                        array_push($categoryMongo, $curr_cat_parent->name);
                        array_push($categoryDraft, [$curr_cat_parent->name, $curr_cat_parent->type]);
                    }
                    $curr_cat_parent = CategoryNew::where('ps_cat_id', $curr_cat_parent->parent_id)->first();
                }
            } else {
                print_r('category trouble maybe null for product: ' . $product->ps_product_id . "\n");
                //var_dump($curr_cat_parent);
                //var_dump($categoryDraft);

            }
            //====================================
        }
        $arrProdBrand = [];
        $arrProdBrandAll = [];
        $arrProdProduct = [];
        $arrProdHair = [];
        $arrProdSub = [];
        $arrProdSubSub = [];
        //if ($categoryDraft){
            foreach ($categoryDraft as $item) {
                if ($item[1] == 'product'){
                    array_push($arrProdProduct, $item[0]);
                    $categoryMongoObj['products'] = $arrProdProduct;
                };
                if ($item[1] == 'hair'){
                    array_push($arrProdHair, $item[0]);
                    $categoryMongoObj['hair'] = $arrProdHair;
                };
                if ($item[1] == 'brand'){
                    array_push($arrProdBrand, $item[0]);
                    array_push($arrProdBrandAll, $item[0]);
                    $categoryMongoObj['brand'] = $arrProdBrand;
                    $categoryMongoObj['brands'] = $arrProdBrandAll;
                };
                if ($item[1] == 'subbrand'){
                    array_push($arrProdSub, $item[0]);
                    $categoryMongoObj['subbrand'] = $arrProdSub;
                    array_push($arrProdBrandAll, $item[0]);
                    $categoryMongoObj['brands'] = $arrProdBrandAll;
                };
                if ($item[1] == 'subsubbrand'){
                    array_push($arrProdSubSub, $item[0]);
                    $categoryMongoObj['subsubbrand'] = $arrProdSubSub;
                    array_push($arrProdBrandAll, $item[0]);
                    $categoryMongoObj['brands'] = $arrProdBrandAll;
                };
            }
        //} else print_r('jjjjjjjj');
        return ['categoryMongo'=>$categoryMongo, 'categoryMongoObj'=>$categoryMongoObj];

    }


    private function getFeatures($product){
        $featuresMongo = [];
        $features_ids = explode(';', $product->ids_features_char);

        for($i = 0; $i < count($features_ids)-1; $i+=2){

            $feature_name = ProductFeatures::where('ps_feature_id', $features_ids[$i])->get();

            if ( isset( $feature_name[0] )) {
                $feature_name = $feature_name[0]->name;
            } else {
                $feature_name = null;
                print_r("feat: " . $features_ids[$i] . "\n");
            }

            $feature_value = ProductFeaturesValues::where('ps_feature_value_id',
                $features_ids[$i+1])->get();

            if ( isset( $feature_value[0] )) {
                $feature_value = $feature_value[0]->value;
            } else {
                $feature_value = null;
                print_r("feat val: " . $features_ids[$i] . "\n");
            }

            if ($feature_name and $feature_value){
                $featuresMongo[$feature_name] = $feature_value;
            }
        }
        return $featuresMongo;
    }

    private function updateCategories($categoryMongo, $categoryMongoObj, $featuresMongo){
        if (array_key_exists('Genre', $featuresMongo)){
            array_push($categoryMongo, $featuresMongo['Genre']);
            $categoryMongoObj['gender'] =[$featuresMongo['Genre']];
        }
        return ['categoryMongo' => $categoryMongo, 'categoryMongoObj' => $categoryMongoObj];
    }

    private function getQty($product){
        $stock = Stock::where('ps_stock_id', $product->id_stock)->get();
        if ($stock->count() > 0){
            return $stock[0]->quantity;
        } else {
            return 0;
        }
    }


    public function makeMongoProduct($product){
        //var_dump($product->ps_product_id);
        $categoriesMongo = $this->getCategories($product);
        $featuresMongo = $this->getFeatures($product);
        $categoriesMongo = $this->updateCategories(
            $categoriesMongo['categoryMongo'],
            $categoriesMongo['categoryMongoObj'],
            $featuresMongo
        );
        $qty = $this->getQty($product);
        //dd($qty);

        $taxMongo = Tax::parseTax($product->id_tax_rules_group);

        ProductMongo::makeProductMongo(
            $product,
            $categoriesMongo['categoryMongo'],
            $categoriesMongo['categoryMongoObj'],
            $featuresMongo,
            $taxMongo,
            $qty
        );
    }

    public function updateMongoProduct($product){
        //var_dump($product->ps_product_id);
        $categoriesMongo = $this->getCategories($product);
        $featuresMongo = $this->getFeatures($product);
        $categoriesMongo = $this->updateCategories(
            $categoriesMongo['categoryMongo'],
            $categoriesMongo['categoryMongoObj'],
            $featuresMongo
        );

        $taxMongo = Tax::parseTax($product->id_tax_rules_group);
        $qty = $this->getQty($product);

        ProductMongo::updateProductMongo(
            $product,
            $categoriesMongo['categoryMongo'],
            $categoriesMongo['categoryMongoObj'],
            $featuresMongo,
            $taxMongo,
            $qty
        );
        CartMongo::updateProductInCarts($product);
        FavoritesMongo::updateProductInFavs($product);
    }

    public function test(){
        $product = ProductNew::where('ps_product_id', "2786")->firstOrFail();
        CartMongo::updateProductInCarts($product);
    }

    public function deleteMongoProduct($idPresta){
        ProductMongo::deleteProductMongo($idPresta);
    }

    public function makeMongoProducts(){
        ProductMongo::truncate();
        $products  = ProductNew::all();
        foreach ($products as $product){
            $this->makeMongoProduct($product);
        }
    }

    public function makeMongoGalleries(){
        GalleriesMongo::truncate();
        foreach ($this->galleries as $gallery){
            $this->makeMongoGallery($gallery[0], $gallery[1], $gallery[2], $gallery[3]);
        }
    }


    private function makeMongoGallery($idCat, $alt, $name, $type){
        $ids =  explode(';', CategoryNew::where('ps_cat_id', $idCat)->get()[0]->ids_prods);
        $products=[];
        foreach ($ids as $id){
            $productMongo = ProductMongo::where('idPresta', '=', (int)$id)->get()[0]->toArray();
            array_push($products, $productMongo['_id'] );
        }
        GalleriesMongo::makeGalleryMongo($alt, $name, $type, $products);
    }

    public function makeMongoCategories(){
        CategoryMongo::truncate();
        $arr_type = [
            'brand'=>'brands',
            'hair'=>'hair-types',
            'product'=> 'product-types',
            'material'=> 'materials'
        ];

        foreach ($arr_type as $prod_key => $prod_type ){
            $cats = CategoryNew::where('type', $prod_key)
                -> get();

            foreach ($cats as $cat){
                CategoryMongo::makeCategoryMongo(
                    $cat->name,
                    $arr_type[$prod_key],
                    $arr_type[$prod_key] . ' ' . $cat->name,
                    []
                );
            }
        }

        $cats = CategoryNew::where('type', 'subbrand')
            -> get();
        foreach ($cats as $cat){
            $parent =  CategoryNew::where('ps_cat_id', $cat->parent_id)->get()[0]->name;
            CategoryMongo::makeCategoryMongo(
                $cat->name,
                $parent,
                $parent . ' ' . $cat->name,
                ['brands']
            );
        }

        $cats = CategoryNew::where('type', 'subsubbrand')
            -> get();
        foreach ($cats as $cat){
            $parent =  CategoryNew::where('ps_cat_id', $cat->parent_id)->get()[0];
            $ancestor = CategoryNew::where('ps_cat_id', $parent->parent_id)->get()[0];
            CategoryMongo::makeCategoryMongo(
                $cat->name,
                $parent->name,
                $ancestor->name . ' ' . $parent->name . ' ' . $cat->name,
                ['brands', $ancestor->name]
            );
        }

        $cats = CategoryNew::where('type', 'submaterial')
            -> get();
        foreach ($cats as $cat){
            $parent =  CategoryNew::where('ps_cat_id', $cat->parent_id)->get()[0]->name;
            CategoryMongo::makeCategoryMongo(
                $cat->name,
                $parent,
                $title = $parent . ' ' . $cat->name,
                ['materials']
            );
        }

        $cats = ['Homme', 'Femme', "Enfant"];
        foreach ($cats as $cat){
            $parent =  "gender";
            CategoryMongo::makeCategoryMongo(
                $cat,
                $parent,
                $title = $parent . ' ' . $cat,
                []
            );
        }

        $cats = CategoryNew::where('type', 'brand')
            -> get();
        foreach ($cats as $cat) {
            $childs = CategoryNew::where('parent_id', $cat->ps_cat_id)->get()->toArray();
            if (count($childs) == 0){
                CategoryMongo::makeCategoryMongo(
                    $cat->name,
                    $cat->name,
                    $cat->name . ' ' . $cat->name,
                    ['brands']
                );
            }
        }

    }

    public function makeMongoCountry(){
        CountryMongo::truncate();
        $countries  = Country::all();
        foreach ($countries as $country){
           CountryMongo:: makeCountryMongo($country);
        }

    }
}