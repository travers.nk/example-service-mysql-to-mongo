<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/quantity/update',
        '/product/add',
        '/product/save',
        '/product/update',
        '/product/delete',
        '/update-product',
        '/add-product',
        '/delete-product'
    ];
}
