<?php

namespace App\Models;

//use Moloquent\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class ProductMongo extends Eloquent {

    protected $connection = 'mongodb';
    protected $collection = 'products_presta';

    private static function setData($productMongo,
                             $product,
                             $categoryMongo,
                             $categoryMongoObj,
                             $featuresMongo,
                             $taxMongo,
                             $qty)
    {
        $productMongo->categories = $categoryMongo;
        $productMongo->categoriesObj = $categoryMongoObj;
        $productMongo->description = $product->description;
        $productMongo->description_short = $product->description_short;
        $productMongo->idPresta =$product->ps_product_id;
        $productMongo->imgPresta = explode(';',  $product->ids_images_char);
        $productMongo->manufacturerDetails = [
            'barCode1' => $product->ean13,
            'barCode2' => $product->ean13_2,
            'manufacturerName' => $product->manufacturer_name
        ];
        $priceDiscount =  $product->price * ( 1 + $taxMongo/100 );
        if ($product->prix_public != 0){
            $discount = (($product->prix_public - $priceDiscount)/$product->prix_public)*100;
        } else $discount = 0;
        $productMongo->pricing = [
            'price' => $product->prix_public,
            'priceDiscount' => $priceDiscount,
            'priceDiscountNoTax' => $product->price,
            'tax' => $taxMongo,
            'discount' => (int)$discount
        ];
        //$productMongo->quantity = $product->quantity;
        $productMongo->quantity = $qty;
        if (array_key_exists('Format', $featuresMongo)) {
            $productMongo->shippingDetails = ['format' => $featuresMongo['Format']];
        } else $productMongo->shippingDetails = ['format' => '0'];

        $productMongo->sku =  $product->reference;
        $productMongo->name =  $product->name;
        $productMongo->active =  $product->active;
        $productMongo->action =  $product->action;
        $productMongo->application =  $product->application;
        $productMongo->date_addPresta =  $product->date_add;
        $productMongo->date_updPresta =  $product->date_upd;
        $productMongo->id_shop_default =  $product->id_shop_default;
        $productMongo->id_category_default =  $product->id_category_default;
        $productMongo->id_default_image =  $product->id_default_image;
        $productMongo->position_in_category =  $product->position_in_category;
        $productMongo->h1_prod =  $product->h1_prod;
        $productMongo->features = $featuresMongo;

        return $productMongo;
    }

    static function makeProductMongo($product, $categoryMongo, $categoryMongoObj, $featuresMongo, $taxMongo, $qty){
        $productMongo = self::setData(
            new ProductMongo(),
            $product,
            $categoryMongo,
            $categoryMongoObj,
            $featuresMongo,
            $taxMongo,
            $qty) ;
        $productMongo->save();
    }

    static function updateProductMongo($product, $categoryMongo, $categoryMongoObj, $featuresMongo, $taxMongo, $qty){
        $productMongo = self::setData(
            ProductMongo::where('idPresta', (int)$product->ps_product_id)->firstOrFail(),
            $product,
            $categoryMongo,
            $categoryMongoObj,
            $featuresMongo,
            $taxMongo,
            $qty
        );
        $productMongo->save();
    }

    static function deleteProductMongo($idPresta){
        ProductMongo::where('idPresta', (int)$idPresta)->delete();
    }
}