<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;


class AuthHeader
{

    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $key = config('auth-header.key');
        $key_to_compare = "Basic " . base64_encode($key.':');
        $access_token = $request->header('Authorization');
        if ($key_to_compare != $access_token){
            throw new AuthenticationException;
        }
        return $next($request);
    }

}