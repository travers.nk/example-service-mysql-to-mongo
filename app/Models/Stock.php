<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stock';

    static function createStockProd($pro_desc){
        $stock_prod = new Stock();
        $stock_prod->ps_stock_id = (int)$pro_desc->stock_available->id;
        $stock_prod->ps_product_id = (int)$pro_desc->stock_available->id_product;
        $stock_prod->ps_shop_id = (int)$pro_desc->stock_available->id_shop;
        $stock_prod->quantity = (int)$pro_desc->stock_available->quantity;
        $stock_prod->save();
    }


    static function updateStockProd($pro_desc){
        $stock_prod = Stock::where('ps_stock_id', $pro_desc->stock_available->id)->firstOrFail();
        $stock_prod->ps_product_id = (int)$pro_desc->stock_available->id_product;
        $stock_prod->ps_shop_id = (int)$pro_desc->stock_available->id_shop;
        $stock_prod->quantity = (int)$pro_desc->stock_available->quantity;
        $stock_prod->save();
    }
}