<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use App\Services\MongoService;

class MongoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \App::bind('MongoService', function() {
            return new MongoService;
        });
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}