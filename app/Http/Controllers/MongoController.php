<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Models\ProductNew;
use App\Models\Stock;
use App\Facades\HttpApiService;
use App\Facades\MongoService;
use Illuminate\Http\Request;

ini_set('max_execution_time', 1900);

class MongoController extends BaseController{

    public function makeProductMongo(){
        MongoService::makeMongoProducts();
        return response()->json(["status"=>200, "message"=>"done"]);
    }

    public function makeGalleryMongo(){
        MongoService::makeMongoGalleries();
        return response()->json(["status"=>200, "message"=>"done"]);
    }

    public function makeCategoryMongo(){
        MongoService::makeMongoCategories();
        return response()->json(["status"=>200, "message"=>"done"]);
    }

    public function makeAll(){
        $this->makeProductMongo();
        $this->makeCategoryMongo();
        $this->makeGalleryMongo();
        $this->makeCountryMongo();

        return response()->json(["status"=>200, "message"=>"done"]);
    }

    public function addProduct(Request $request){
        $text = '-----add from service----';
        file_put_contents("/var/log/presta/fileservice.log", print_r($text, true) . PHP_EOL, FILE_APPEND);
        file_put_contents("/var/log/presta/fileservice.log", print_r($request->id_product, true) . PHP_EOL, FILE_APPEND);

        $response = HttpApiService::getProduct($request->id_product);
        ProductNew::createProduct($response);
        $product = ProductNew::where('ps_product_id', $request->id_product)->firstOrFail();
        MongoService::makeMongoProduct($product);
        return response()->json(["status"=>201, "id" => $request->id_product]);
    }

    public function updateProduct(Request $request){
        $text = '-----update from service----';
        file_put_contents("/var/log/presta/fileservice.log", print_r($text, true) . PHP_EOL, FILE_APPEND);
        file_put_contents("/var/log/presta/fileservice.log", print_r($request->id_product, true) . PHP_EOL, FILE_APPEND);
        $response = HttpApiService::getProduct($request->id_product);
        ProductNew::updateProduct($response);
        $product = ProductNew::where('ps_product_id', $request->id_product)->firstOrFail();
        $response = HttpApiService::getStock($product->id_stock);
        Stock::updateStockProd($response);
        MongoService::updateMongoProduct($product);
        return response()->json(["status"=>200, "id" => $request->id_product]);
    }

    public function deleteProduct(Request $request){
        ProductNew::deleteProduct($request->id_product);
        MongoService::deleteMongoProduct($request->id_product);
        return response()->json(["status"=>204, "id" => $request->id_product]);
    }


    public function test(Request $request){
        MongoService::test();
        return response()->json(["status"=>200]);
    }

    public function makeCountryMongo(){
        MongoService::makeMongoCountry();
        return response()->json(["status"=>200]);
    }

}