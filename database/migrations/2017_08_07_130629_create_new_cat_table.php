<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewCatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_new', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('ps_cat_id')->unique();
            $table->integer('parent_id');
            $table->integer('level_depth');
            $table->string('name');
            $table->string('type')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cat_new');
    }
}
