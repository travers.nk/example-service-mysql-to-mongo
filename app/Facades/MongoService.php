<?php
namespace App\Facades;
use Illuminate\Support\Facades\Facade;


class MongoService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'MongoService';
    }
}