<?php

namespace App\Models;

//use Moloquent\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CountryMongo extends Eloquent {

    protected $connection = 'mongodb';
    protected $collection = 'countries';

    static function makeCountryMongo($country){
        $countryMongo = new CountryMongo();
        $countryMongo->ps_country_id = $country->ps_country_id;
        $countryMongo->name = $country->name;
        $countryMongo->save();
    }
}