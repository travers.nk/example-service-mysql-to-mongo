<?php

namespace App\Services;

use App\Models\ProductNew;
use App\Models\ProductFeatures;
use App\Models\ProductFeaturesValues;
use App\Models\Manufacturer;
use App\Models\CategoryNew;
use App\Models\Tax;
use App\Models\Stock;
use App\Models\Country;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class HttpApiService
{
    private $url = '';
    //'https://www.peyrouse-hair-shop.com/api/';
    //private $url= 'http://localhost:8000/webservice/dispatcher.php?url=';
    private $headerA = ''; //  'Authorization';
    private $headerB = ''; //'Basic RUtBVjdNS1hCVjQ0NTRMMlU3NElYNTVZTkxZTllNMkg6';

    private function setParams(){
        $this->url = config('services.http.urlApi');
        $this->headerA = config('services.http.headerA');
        $this->headerB = config('services.http.headerB');
    }

    private function sendGetRequest($resource){
        $this->setParams();
        $response = '';
        if (isset ($this->url) && ($this->url.$resource === $this->url."categories/104")
            || ($this->url.$resource === $this->url."categories/436")
            || ($this->url.$resource === $this->url."categories/437")
            || ($this->url.$resource === $this->url."categories/459")
            || ($this->url.$resource === $this->url."categories/460")
            || ($this->url.$resource === $this->url."categories/461")
        ) {
        } else {
            $response =  \Httpful\Request::get($this->url . $resource)
                ->addHeader($this->headerA, $this->headerB)
                ->expectsXml()
                ->send();
        }
        return $response;
    }

    private function getIds($attrs){
        $ids = [];
        foreach ($attrs as $item) {
            $ids[] = $item->attributes()->id->__toString();
        }
        return $ids;
    }

    public function getProduct($value){
        if ($value == '' ){
            throw new NotFoundHttpException;
        }
        $response = $this->sendGetRequest('products/'.$value);
        $pro_desc = new \SimpleXMLElement($response->raw_body, LIBXML_NOCDATA);
        $cat_str = '';
        $feat_str = '';
        $img_str = '';
        foreach ($pro_desc->product->associations->categories->category as $item){
            $cat_str = $cat_str . ';' . $item->id->__toString();
        }
        foreach ($pro_desc->product->associations->images->image as $item){
            $img_str = $img_str . ';' . $item->id->__toString();
        }
        foreach ($pro_desc->product->associations->product_features->product_feature as $item){
            $feat_str = $feat_str . ';' . $item->id->__toString() . ';'
                . $item->id_feature_value->__toString();
        }
        //dd($pro_desc);
        return [
            'pro_desc' => $pro_desc,
            'cat_str' => $cat_str,
            'img_str' => $img_str,
            'feat_str' => $feat_str
        ];
    }

    public function getProducts(){
        ProductNew::truncate();
        $response  = $this->sendGetRequest('products');
        //dd($response);
        $ids = $this->getIds($response->body->products->product);
        foreach ($ids as $key => $value) {
            //dd($value);
            $response = $this->getProduct($value);
            //dd($response);
            ProductNew::createProduct($response);
        }
    }


    public function getStock($value){
        $response = $this->sendGetRequest('stock_availables/'.$value);
        $pro_desc = new \SimpleXMLElement($response->raw_body, LIBXML_NOCDATA);
        return $pro_desc;
    }


    public function getStocks(){
        Stock::truncate();
        $response  = $this->sendGetRequest('stock_availables');
        $ids = $this->getIds($response->body->stock_availables->stock_available);
        foreach ($ids as $key => $value) {
            $response = $this->getStock($value);
            Stock::createStockProd($response);
        }

    }

    public function getProductFeatures(){
        ProductFeatures::truncate();
        $response  = $this->sendGetRequest('product_features');
        $ids = $this->getIds($response->body->product_features->product_feature);
        foreach ($ids as $key => $value) {
            $response = $this->sendGetRequest('product_features/'.$value);
            $pro_desc = new \SimpleXMLElement($response->raw_body, LIBXML_NOCDATA);
            ProductFeatures::createFeats($pro_desc);
        }
    }

    public function getProductFeaturesValues(){
        ProductFeaturesValues::truncate();
        $response  = $this->sendGetRequest('product_feature_values');
        $ids = $this->getIds($response->body->product_feature_values->product_feature_value);
        foreach ($ids as $key => $value) {
            $response = $this->sendGetRequest('product_feature_values/'.$value);
            $pro_desc = new \SimpleXMLElement($response->raw_body, LIBXML_NOCDATA);
            ProductFeaturesValues::createFeatsValues($pro_desc);
        }
    }

    public function getTax(){
        Tax::truncate();
        $response  = $this->sendGetRequest('tax_rule_groups');
        $ids = $this->getIds($response->body->tax_rule_groups->tax_rule_group);
        foreach ($ids as $key => $value) {
            $response = $this->sendGetRequest('tax_rule_groups/'.$value);
            $pro_desc = new \SimpleXMLElement($response->raw_body, LIBXML_NOCDATA);
            Tax::createTax($pro_desc);
        }
    }

    public function getManufacturer(){
        Manufacturer::truncate();
        $response  = $this->sendGetRequest('manufacturers');
        $ids = $this->getIds($response->body->manufacturers->manufacturer);
        foreach ($ids as $key => $value) {
            $response = $this->sendGetRequest('manufacturers/'.$value);
            $pro_desc = new \SimpleXMLElement($response->raw_body, LIBXML_NOCDATA);
            Manufacturer::createManuf($pro_desc);
        }
    }

    public function getCategories()
    {
        CategoryNew::truncate();
        $response = $this->sendGetRequest('categories');
            $ids = $this->getIds($response->body->categories->category);
            foreach ($ids as $key => $value) {
                $response = $this->sendGetRequest('categories/' . $value);
                if ($response != ''){
                    $pro_desc = new \SimpleXMLElement($response->raw_body, LIBXML_NOCDATA);
                    $products_str ='';
                    $cats_str ='';

                    foreach ($pro_desc->category->associations->products->product as $item){
                        $products_str = $products_str . ';' . $item->id->__toString();
                    }

                    foreach ($pro_desc->category->associations->categories->category as $item){
                        $cats_str = $cats_str . ';' . $item->id->__toString();
                    }
                    CategoryNew::createCategory($pro_desc, $products_str, $cats_str);
                }
            }
        CategoryNew::setCatType();
    }


    public function getCountry($value){
        $response = $this->sendGetRequest('countries/'.$value);
        $pro_desc = new \SimpleXMLElement($response->raw_body, LIBXML_NOCDATA);
        return $pro_desc;
    }


    public function getCountries(){
        Country::truncate();
        $response  = $this->sendGetRequest('countries');
        //dd($response);
        $ids = $this->getIds($response->body->countries->country);
        foreach ($ids as $key => $value) {
            $response = $this->getCountry($value);
            Country::createCountry($response);
        }

    }
}