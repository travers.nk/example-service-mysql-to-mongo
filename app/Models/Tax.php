<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $table = 'tax';
    protected $connection = 'mysql';

    static function createTax($pro_desc){
        $tax = new Tax();
        $tax->ps_tax_id =     (int)$pro_desc->tax_rule_group->id;
        $tax->name =          $pro_desc->tax_rule_group->name;
        $tax->active =        (int)$pro_desc->tax_rule_group->active;
        $tax->deleted =       (int)$pro_desc->tax_rule_group->deleted;
        $tax->save();
    }

    static function parseTax($id){
        $tax_item = Tax::where('ps_tax_id', $id)->get()[0];
        preg_match('/\(.*\)/', $tax_item->name, $matches);
        $a = ltrim($matches[0], '(');
        return (float)rtrim($a, '%)');
    }
}