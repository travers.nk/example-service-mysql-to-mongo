<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Facades\HttpApiService;

ini_set('max_execution_time', 1900);

class HttpApiController extends BaseController{

    public function getProducts(){
        HttpApiService::getProducts();
        return response()->json(["status"=>200, "message"=>"done"]);
    }

    public function getCategories(){
        HttpApiService::getCategories();
        return response()->json(["status"=>200, "message"=>"done"]);

    }

    public function getProductFeatures(){
        HttpApiService::getProductFeatures();
        return response()->json(["status"=>200, "message"=>"done"]);
    }

    public function getProductFeaturesValues(){
        HttpApiService::getProductFeaturesValues();
        return response()->json(["status"=>200, "message"=>"done"]);
    }

    public function getTax(){
        HttpApiService::getTax();
        return response()->json(["status"=>200, "message"=>"done"]);
    }

    public function getManufacturer(){
        HttpApiService::getManufacturer();
        return response()->json(["status"=>200, "message"=>"done"]);
    }

    public function getStock(){
        HttpApiService::getStocks();
        return response()->json(["status"=>200, "message"=>"done"]);
    }

    public function getCountry(){
        HttpApiService::getCountries();
        return response()->json(["status"=>200, "message"=>"done"]);
    }


    public function getAll(){
        $this->getTax();
        $this->getProductFeatures();
        $this->getProductFeaturesValues();
        $this->getManufacturer();
        $this->getCategories();
        $this->getStock();
        $this->getCountry();
        $this->getProducts();
    }
}

