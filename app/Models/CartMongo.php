<?php

namespace App\Models;

//use Moloquent\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CartMongo extends Eloquent
{

    protected $connection = 'mongodb';
    protected $collection = 'cart';

    static function updateProductInCarts($productPresta){
        $productMongo = ProductMongo::where('idPresta', (int)$productPresta->ps_product_id)->firstOrFail();
        $ids_cart = $productMongo->in_carts;
        if ($ids_cart){
            foreach ($ids_cart as $id_cart){
                $cart = CartMongo::where('_id', $id_cart['id'])->firstOrFail();
                $products = $cart->products;
                $i=0;
                foreach ($products as $product){
                    if ($product['id'] == $productMongo->_id){
                        $old_priceDiscount = $product['item']['pricing']['priceDiscount'];
                        $old_priceDiscountNoTax = $product['item']['pricing']['priceDiscountNoTax'];
                        $old_ttc =  $old_priceDiscount - $old_priceDiscountNoTax;
                        $new_ttc = $productMongo['pricing']['priceDiscount'] - $productMongo['pricing']['priceDiscountNoTax'];

                        $cart->total = $cart->total - $old_priceDiscount * $product['quantity']
                            + $productMongo['pricing']['priceDiscount'] * $product['quantity'];

                        $cart->totalTTC = $cart->totalTTC - $old_ttc * $product['quantity']
                            + $new_ttc * $product['quantity'];

                        $products[$i]['item'] = $productMongo->toArray();
                        break;
                    }
                    $i++;
                }
                $cart->products = $products;
                $cart->save();
            }
        }
    }
}