<?php

namespace App\Models;

//use Moloquent\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CategoryMongo extends Eloquent {

    protected $connection = 'mongodb';
    protected $collection = 'categories_presta';

    static function makeCategoryMongo($path, $parent, $title, $ancestors){
        $categoryMongo = new CategoryMongo();
        $categoryMongo->path = $path;
        $categoryMongo->parent = $parent;
        $categoryMongo->title = $title;
        $categoryMongo->ancestors = $ancestors;
        $categoryMongo->save();
    }
}