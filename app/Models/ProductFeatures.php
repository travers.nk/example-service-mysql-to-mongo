<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProductFeatures extends Model
{
    protected $table = 'products_features';

    static function createFeats($pro_desc){
        $feature = new ProductFeatures();
        $feature->ps_feature_id = (int)$pro_desc->product_feature->id;
        $feature->position = (int)$pro_desc->product_feature->position;
        $feature->name = $pro_desc->product_feature->name->language;
        $feature->save();
    }
}