<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullProductsNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products_new', function (Blueprint $table) {
            $table->integer('id_manufacturer')->nullable()->change();
            $table->integer('id_category_default')->nullable()->change();
            $table->string('id_default_image')->nullable()->change();
            $table->text('meta_description')->nullable()->change();
            $table->text('meta_title')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->text('description_short')->nullable()->change();
            $table->string('id_tax_rules_group')->nullable()->change();
            $table->string('position_in_category')->nullable()->change();
            $table->string('manufacturer_name')->nullable()->change();
            $table->float('quantity')->nullable()->change();
            $table->float('price')->nullable()->change();
            $table->float('prix_public')->nullable()->change();
            $table->integer('active')->nullable()->change();
            $table->text('h1_prod')->nullable()->change();
            $table->string('name')->nullable()->change();
            $table->string('ids_category_char')->nullable()->change();
            $table->string('ids_images_char')->nullable()->change();
            $table->string('ids_features_char')->nullable()->change();
            $table->integer('id_shop_default')->nullable()->change();
            $table->string('reference')->nullable()->change();
            $table->string('ean13')->nullable()->change();
            $table->string('ean13_2')->nullable()->change();
            $table->string('date_add')->nullable()->change();
            $table->string('date_upd')->nullable()->change();
            $table->string('action')->nullable()->change();
            $table->string('application')->nullable()->change();
            //string ; delimeter

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_new', function (Blueprint $table) {
            //
        });
    }
}
