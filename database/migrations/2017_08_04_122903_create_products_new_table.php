<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_new', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('id_manufacturer');
            $table->integer('id_category_default');
            $table->char('id_default_image');
            $table->text('meta_description');
            $table->text('meta_title');
            $table->text('description');
            $table->text('description_short');
            $table->char('id_tax_rules_group');
            $table->char('position_in_category');
            $table->char('manufacturer_name');
            $table->float('quantity');
            $table->float('price');
            $table->float('prix_public');
            $table->integer('active');
            $table->text('h1_prod');
            //string ; delimeter
            $table->text('ids_category');
            $table->text('ids_images');
            $table->text('ids_features');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products_new');
    }
}
