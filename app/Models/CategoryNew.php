<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CategoryNew extends Model
{
    protected $table = 'cat_new';
    const idsBrand = [11, 12, 445, 122, 127, 362, 105, 107, 142, 149, 329, 441, 507, 355, 509];
    const idsMaterial = [40, 39, 42, 123, 41, 45, 16, 15, 449];
    const idShopNail =  289;
    const idProductCat = 395;
    const idHairCat = 371;

    private static function setHairProductType(){
        CategoryNew::where('parent_id', self::idProductCat)->update(['type' => 'product']);
        CategoryNew::where('parent_id', self::idHairCat)->update(['type' => 'hair']);
    }
    //CategoryNew::where('ps_cat_id', 371)->update(['type' => 'hair']); ??
    //CategoryNew::where('ps_cat_id', 395)->update(['type' => 'product']); ??

    private static function setTypeforBrandsMaterials($ids, $type_name){
        foreach($ids as $id){
            CategoryNew::where('parent_id', $id)->update(['type' => $type_name]);
        }
    }

    private static function setSubSub(){
        $subbrands = CategoryNew::where('type', 'subbrand')
            ->get();
        foreach ($subbrands as $subbrand){
            CategoryNew::where('parent_id', $subbrand->ps_cat_id)->update(['type' => 'subsubbrand']);
        }
    }

    private static function setNail(){
        CategoryNew::where('parent_id', self::idShopNail)->update(['type' => 'nail']);
        $subnails = CategoryNew::where('type', 'nail')
            ->get();
        foreach ($subnails as $subnail){
            CategoryNew::where('parent_id', $subnail->ps_cat_id)->update(['type' => 'subnail']);
        }
        $subsubnails = CategoryNew::where('type', 'subnail')
            ->get();
        foreach ($subsubnails as $subsubnail){
            CategoryNew::where('parent_id', $subsubnail->ps_cat_id)->update(['type' => 'subsubnail']);
        }

        $subsubsubnails = CategoryNew::where('type', 'subsubnail')
            ->get();
        foreach ($subsubsubnails as $subsubsubnail){
            CategoryNew::where('parent_id', $subsubsubnail->ps_cat_id)->update(['type' => 'subsubsubnail']);
        }
        $subsubsubsubnails = CategoryNew::where('type', 'subsubsubnail')
            ->get();
        foreach ($subsubsubsubnails as $subsubsubsubnail){
            CategoryNew::where('parent_id', $subsubsubsubnail->ps_cat_id)->update(['type' => 'subsubsubsubnail']);
        }
    }

    static function createCategory($pro_desc, $products_str, $cats_str ){
        $cat = new CategoryNew();
        $cat->ps_cat_id =   (int)$pro_desc->category->id;
        $cat->parent_id =   (int)$pro_desc->category->id_parent;
        $cat->name =        $pro_desc->category->name->language;
        $cat->level_depth = $pro_desc->category->level_depth;
        if (in_array((int)$pro_desc->category->id, self::idsBrand)) {
            $cat->type = 'brand';
        } else if (in_array((int)$pro_desc->category->id, self::idsMaterial)) {
            $cat->type = 'material';
        }
        $cat->ids_prods =  ltrim($products_str,';');
        $cat->ids_cats =  ltrim($cats_str, ';');
        $cat->save();
    }

    static function setCatType(){
        self::setHairProductType();
        self::setTypeforBrandsMaterials(self::idsBrand, 'subbrand');
        self::setTypeforBrandsMaterials(self::idsMaterial, 'submaterial');
        self::setSubSub();
        self::setNail();
    }
}