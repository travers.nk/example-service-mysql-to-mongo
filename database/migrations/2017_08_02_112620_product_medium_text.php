<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductMediumText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->mediumText('imgUrlPresta')->change();
            $table->mediumText('brands')->change();
            $table->mediumText('product_type')->change();
            $table->mediumText('hair')->change();
            $table->mediumText('gender')->change();
            $table->mediumText('tags')->change();
            $table->mediumText('materiel')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->char('imgUrlPresta')->change();
            $table->char('brands')->change();
            $table->char('format')->change();
            $table->char('product_type')->change();
            $table->char('hair')->change();
            $table->char('gender')->change();
            $table->char('tags')->change();
            $table->char('materiel')->change();
        });
    }
}
