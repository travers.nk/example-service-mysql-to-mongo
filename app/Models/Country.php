<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    static function createCountry($pro_desc){
        $country = new Country();
        $country->ps_country_id = (int)$pro_desc->country->id;
        $country->name =  $pro_desc->country->name->language;
        $country->save();
    }
}