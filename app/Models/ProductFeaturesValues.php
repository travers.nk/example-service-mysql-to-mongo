<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProductFeaturesValues extends Model
{
    protected $table = 'products_features_values';

    static function createFeatsValues($pro_desc){
        $feature = new ProductFeaturesValues();
        $feature->ps_feature_value_id =(int) $pro_desc->product_feature_value->id;
        $feature->id_feature = (int)$pro_desc->product_feature_value->id_feature;
        $feature->position = (int)$pro_desc->product_feature_value->position;
        $feature->custom =  $pro_desc->product_feature_value->custom;
        $feature->value = $pro_desc->product_feature_value->value->language;
        $feature->save();
    }
}