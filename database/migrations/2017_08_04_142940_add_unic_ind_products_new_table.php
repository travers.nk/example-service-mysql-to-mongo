<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnicIndProductsNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products_new', function (Blueprint $table) {
            $table->unique('ps_product_id');
            $table->integer('id_shop_default');
            $table->char('reference');
            $table->char('ean13');
            $table->char('ean13_2');
            $table->char('date_add');
            $table->char('date_upd');
            $table->char('action');
            $table->char('application');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_new', function (Blueprint $table) {
            //
        });
    }
}
