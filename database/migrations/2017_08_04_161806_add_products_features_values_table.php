<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductsFeaturesValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_features_values', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('ps_feature_value_id')->unigue();
            $table->integer('position')->nullable();
            $table->integer('id_feature')->nullable();
            $table->string('value')->nullable();
            $table->string('custom')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products_features_values');
    }
}
