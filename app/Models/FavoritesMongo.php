<?php

namespace App\Models;

//use Moloquent\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FavoritesMongo extends Eloquent
{

    protected $connection = 'mongodb';
    protected $collection = 'favorites';

    static function updateProductInFavs($productPresta){
        $productMongo = ProductMongo::where('idPresta', (int)$productPresta->ps_product_id)->firstOrFail();
        $ids_favs = $productMongo->in_favs;
        if ($ids_favs){
            foreach ($ids_favs as $id_fav){
                $fav = FavoritesMongo::where('_id', $id_fav['id'])->firstOrFail();
                $products = $fav->products;
                $i=0;
                foreach ($products as $product){
                    if ($product['id'] == $productMongo->_id){
                        $products[$i]['item'] = $productMongo->toArray();
                        break;
                    }
                    $i++;
                }
                $fav->products = $products;
                $fav->save();
            }
        }
    }
}